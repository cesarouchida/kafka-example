ACCOUNT=cesarouchida
PROJECT_NAME=manual-kafka
VERSION_TAG=v3

local:
	echo INICIALIZANDO DOCKER LOCAL
	docker-compose -f ./docker/docker-compose.yml up -d

push:
	echo ENVIANDO PARA DOCKER HUB -- MANUAL-KAFKA
	docker build -t $(ACCOUNT)/$(PROJECT_NAME):$(VERSION_TAG) -f docker/Dockerfile .
	docker tag $(ACCOUNT)/$(PROJECT_NAME):$(VERSION_TAG) $(ACCOUNT)/$(PROJECT_NAME):latest
	docker push $(ACCOUNT)/$(PROJECT_NAME):$(VERSION_TAG)
	docker push $(ACCOUNT)/$(PROJECT_NAME)

down:
	echo DESTROY LOCAL
	docker-compose -f .\docker\docker-compose.yml down