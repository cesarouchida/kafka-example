# kafka-example

Exemplo monta uma estrutura com o Apache Kafka localmente com o docker 
### USAGE

Para utilizar localmente 

```
    $ make local     
```

Destruir os containers

```
    $ make down     
```


### UPDATE DOCKER-HUB

Deve estar logado com o _docker login_
```
    $ make push VERSION_TAG=v3     
```

* Versão é opcional